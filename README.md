# cpp-closestneighbours

## Observations

Voici le résultat de l'exécution du test "ClosestPair: Benchmark":

```
Using random points
NaivePairs run in 92 ms
FastPairs run in 24 ms

Using random points
NaivePairs run in 355 ms
FastPairs run in 49 ms

Using random points
NaivePairs run in 2367 ms
FastPairs run in 133 ms

Using worst case points
NaivePairs run in 191 ms
FastPairs run in 38 ms

Using worst case points
NaivePairs run in 442 ms
FastPairs run in 49 ms

Using worst case points
NaivePairs run in 1707 ms
FastPairs run in 110 ms
```

On voit très clairement que FastPairs est plus rapide que NaivePairs. En effet, comme vu dans la théorie, on passe de
l'algorithme naïf qui est en O(n2) vers un algorithme en O(n log n).

## Exigences

Chaque exigence pas respectée enlève 0.1 de la note du TP.

- [X] Vous devez lancer au moins une exception (et la tester) dans Point
    - La division par zéro se prête bien à ceci
- [X] Vous avez implémenté des tests unitaires pour tester toutes les fonctions de Point
- [X] Le code doit passer tous les tests sans erreurs
- [X] Le Readme.md contient vos observations niveau performance entre l’algorithme naïve et le
  vôtre
    - Il ne s’agit pas d’un rapport, mais des valeurs de vos mesures et quelques phrases
      pour l’interprétation des résultats.
- [X] Votre repository ne doit pas contenir des fichiers binaires et temporaires (comme le
  répertoire cmake-build-debug ou .idea)
- [X] Après chaque commit, le code compile sur le CI de gitlab avec -Wall et -Werror comme
  options et les tests unitaires sont exécutés
- [X] Vous travaillez sur un fork du repo original (lien sur cyberlearn) et devez créer un merge
  request avant la deadline.
    - Beat Wolf doit avoir accès à votre repository (ajouter comme membre ou mettre en
      public)
