//
// Created by beatw on 3/3/2022.
//


#include "../includes/Catch2/catch.hpp"
#include "../src/Point.h"

TEST_CASE("Point: Construction") {
    Point p(10, 12);

    REQUIRE(10 == Approx(p.getX()));
    REQUIRE(12 == Approx(p.getY()));
    REQUIRE("" == p.getName());

    p = Point(-123, 234, "test");
    REQUIRE(-123 == Approx(p.getX()));
    REQUIRE(234 == Approx(p.getY()));
    REQUIRE("test" == p.getName());
}

TEST_CASE("Point: Construction 2") {
    Point p(10, 12);

    Point p2 = p;
    REQUIRE(p.getX() == Approx(p2.getX()));
    REQUIRE(p.getY() == Approx(p2.getY()));
    REQUIRE(p.getName() == p2.getName());
}


TEST_CASE("Point: Construction 3") {
    Point p(10, 12, "Name");

    Point p2 = p;
    REQUIRE(p.getX() == Approx(p2.getX()));
    REQUIRE(p.getY() == Approx(p2.getY()));
    REQUIRE(p.getName() == p2.getName());
}

TEST_CASE("Point: Chaining") {
    Point p(10, 12, "Name");

    (((p *= 10) /= 2) += {20, 10}) -= {5, 3};

    REQUIRE(p.getX() == Approx(65));
    REQUIRE(p.getY() == Approx(67));
}

TEST_CASE("Point: Setter") {
    Point p(10, 12, "Name");

    p.setX(20);
    p.setY(30);

    REQUIRE(p.getX() == Approx(20));
    REQUIRE(p.getY() == Approx(30));
}

TEST_CASE("Point: Operator") {
    Point p1(1, 2, "Name1");
    Point p2(2, 4, "Name2");

    Point p3 = p1 + p2;
    REQUIRE(p3.getX() == Approx(3));

    p3 = p1 - p2;
    REQUIRE(p3.getX() == Approx(-1));

    p3 = p1 * 2;
    REQUIRE(p3.getX() == Approx(2));

    p3 = p1 / 2;
    REQUIRE(p3.getX() == Approx(0.5));
}

TEST_CASE("Division by 0") {
    Point p1(1, 2, "Name1");
    Point p2(2, 4, "Name2");
    Point p3 = p1 / 0;
    Point p4 = p1 /= 0;

    REQUIRE(p3.getX() == Approx(0));
    REQUIRE(p3.getY() == Approx(0));
}

