//
// Created by "Beat Wolf" on 08.02.2022.
//

#include <algorithm>
#include "ClosestPair.h"

ClosestPair::ClosestPair() {
    const auto comp = [](const Point *a, const Point *b) {
        if (a->getY() == b->getY()) {
            return a->getX() < b->getX();
        }
        return a->getY() < b->getY();
    };

    candidates = std::set<Point *, std::function<bool(const Point *, const Point *)>>(comp);
}

std::pair<Point *, Point *> ClosestPair::closestPair(std::vector<Point *> &searchPoints) {
    //Init
    points = searchPoints;
    candidates.clear();
    leftMostCandidate = 0;

    //TODO: Finish init

    //Init solution
    solution = std::make_pair(points[0], points[1]);
    smallestDistance = std::numeric_limits<double>::infinity();


    std::sort(points.begin(), points.end(), [](const Point *a, const Point *b) {
        if (a->getX() == b->getX()) {
            return a->getY() < b->getY();
        }
        return a->getX() < b->getX();
    });

    solution = std::make_pair(points[0], points[1]);
    smallestDistance = solution.first->distance(*solution.second);

    //TODO: Implement main loop to handle events

    while (!points.empty()) {
        Point *point = points.front();
        points.erase(points.begin());
        handleEvent(point);
    }

    return solution;
}

void ClosestPair::handleEvent(Point *p) {
    shrinkCandidates(p);

    //TODO: Find correct search interval in candidates

    lowerSearch = {p->getX(), p->getY() - smallestDistance};
    upperSearch = {p->getX(), p->getY() + smallestDistance};

    auto itLower = candidates.lower_bound(&lowerSearch);
    auto itUpper = candidates.upper_bound(&upperSearch);

    // TODO Check all points in interval

    for (auto it = itLower; it != itUpper; ++it) {
        auto distance = p->distance(**it);
        if (distance < smallestDistance) {
            solution = std::make_pair(p, *it);
            smallestDistance = distance;
        }
    }

    candidates.insert(p);
}

void ClosestPair::shrinkCandidates(const Point *p) {
    while (p->getX() - points[leftMostCandidate]->getX() > smallestDistance) {
        candidates.erase(points[leftMostCandidate]);
        leftMostCandidate++;
    }
}