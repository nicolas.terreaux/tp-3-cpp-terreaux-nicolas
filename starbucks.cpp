//
// Created by "Beat Wolf" on 08.02.2022.
//
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

#include "src/Point.h"
#include "src/ClosestPair.h"


constexpr int kColumnName = 2;
constexpr int kColumnLongitude = 11;
constexpr int kColumnLatitude = 12;

int main() {
    std::string fileName = "../data/us_starbucks.tsv";

    std::vector<double> data, tempData;
    //Initialize the file stream "Starbucks.txt"
    std::ifstream input_file(fileName);

    if (!input_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << fileName << "'" << std::endl;
        return EXIT_FAILURE;
    }

    std::string str;
    getline(input_file, str); //IGNORE THE FIRST LINE (HEADER)
    std::vector<Point *> starbucksPoints;


    while (true) {

        getline(input_file, str);
        if (input_file.eof()) {
            break;
        }
        std::stringstream buffer(str);
        std::vector<std::string> values;
        std::string temp;

        while (getline(buffer, temp, '\t')) {
            values.push_back(temp);
        }

        double latitude = std::stod(values[kColumnLatitude]);
        double longitude = std::stod(values[kColumnLongitude]);
        std::string name = values[kColumnName];

        auto *point = new Point(latitude, longitude, name);
        starbucksPoints.push_back(point);

    }
    std::cout << "Number of starbucks at the beginning: " << starbucksPoints.size() << std::endl;

    // Set the percentage of the data to be used for the test
    int percentage = (starbucksPoints.size() / 100) * 10;
    ClosestPair cp;
    std::pair<Point *, Point *> pair;
    for (int i = 0; i < percentage; i++) {

        pair = cp.closestPair(starbucksPoints);

        //Get the average longitude and latitude between the two points
        double newLongitude = (pair.first->getY() + pair.second->getY()) / 2;
        double newLatitude = (pair.first->getX() + pair.second->getX()) / 2;

        // Create the new name of the starbucks
        std::string newName = pair.first->getName() + " concat with " + pair.second->getName();
        Point *newPoint = new Point(newLatitude, newLongitude, newName);
        starbucksPoints.push_back(newPoint);

        // Erase the old points (first and second of the pair)
        starbucksPoints.erase(std::remove(starbucksPoints.begin(), starbucksPoints.end(), pair.first),
                              starbucksPoints.end());
        starbucksPoints.erase(std::remove(starbucksPoints.begin(), starbucksPoints.end(), pair.second),
                              starbucksPoints.end());

    }


    std::cout << "Number of starbucks at the end: " << starbucksPoints.size() << std::endl;


    // Write the new starbucks to a new tsv file
    std::string outputFileString = "../data/us_starbucks_new.tsv";

    std::ofstream output_file(outputFileString);

    if (!output_file.is_open()) {
        std::cerr << "Could not open the file - '"
                  << outputFileString << "'" << std::endl;
        return EXIT_FAILURE;
    }

    for (auto &point: starbucksPoints) {
        output_file << point->getName() << "\t" << point->getX() << "\t" << point->getY() << std::endl;
    }
    output_file.close();

    return EXIT_SUCCESS;


}

